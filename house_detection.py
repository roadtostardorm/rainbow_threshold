import cv2
import numpy
import logger
from matplotlib import pyplot


def house_detection(source_image_path, destination_image_path):
    original_image = cv2.imread(source_image_path)

    bmat, gmat, rmat = cv2.split(original_image)
    rgb_image = cv2.merge([rmat, gmat, bmat])

    # pyplot.figure()
    # pyplot.imshow(rgb_image), pyplot.title('original_image'), pyplot.axis('off')
    # pyplot.figure()
    # pyplot.subplot(221), pyplot.imshow(rmat, 'gray'), pyplot.title('rmat'), pyplot.axis('off')
    # pyplot.subplot(222), pyplot.imshow(gmat, 'gray'), pyplot.title('gmat'), pyplot.axis('off')
    # pyplot.subplot(223), pyplot.imshow(bmat, 'gray'), pyplot.title('bmat'), pyplot.axis('off')
    # pyplot.subplot(224), pyplot.imshow(rgb_image), pyplot.title('original_image'), pyplot.axis('off')

    levelr = 0.35*255
    levelg = 0.48*255
    levelb = 0.1*255

    rimage = cv2.threshold(rmat, levelr, 255, cv2.THRESH_BINARY)[1]
    gimage = cv2.threshold(gmat, levelg, 255, cv2.THRESH_BINARY)[1]
    bimage = cv2.threshold(bmat, levelb, 255, cv2.THRESH_BINARY)[1]
    sumimage = cv2.bitwise_and(rimage, cv2.bitwise_and(gimage, bimage))
    # pyplot.figure()
    # pyplot.subplot(221), pyplot.imshow(rimage, 'gray'), pyplot.title('r'), pyplot.axis('off')
    # pyplot.subplot(222), pyplot.imshow(gimage, 'gray'), pyplot.title('g'), pyplot.axis('off')
    # pyplot.subplot(223), pyplot.imshow(bimage, 'gray'), pyplot.title('b'), pyplot.axis('off')
    # pyplot.subplot(224), pyplot.imshow(sumimage, 'gray'), pyplot.title('sum'), pyplot.axis('off')

    reverted_image = cv2.threshold(sumimage, 255/2, 255, cv2.THRESH_BINARY_INV)[1]
    # pyplot.figure()
    # pyplot.imshow(reverted_image, 'gray'), pyplot.title('rev'), pyplot.axis('off')

    kernel = numpy.ones((5, 5), numpy.uint8)
    iopen = cv2.morphologyEx(reverted_image, cv2.MORPH_OPEN, kernel)
    kernel = numpy.ones((5, 5), numpy.uint8)
    iopenclose = cv2.morphologyEx(iopen, cv2.MORPH_CLOSE, kernel)
    kernel = numpy.ones((2, 2), numpy.uint8)
    iopenclosedilate = cv2.dilate(iopenclose, kernel, iterations=1)
    iopenclosedialtedilate = cv2.dilate(iopenclosedilate, kernel, iterations=1)
    iopenclosedilatedilateopen = cv2.morphologyEx(iopenclosedialtedilate, cv2.MORPH_OPEN, kernel)
    # pyplot.figure()
    # pyplot.subplot(231), pyplot.imshow(iopen, 'gray'), pyplot.title('open'), pyplot.axis('off')
    # pyplot.subplot(232), pyplot.imshow(iopenclose, 'gray'), pyplot.title('open-close'), pyplot.axis('off')
    # pyplot.subplot(233), pyplot.imshow(iopenclosedilate, 'gray'), pyplot.title('open-close-dilate'), pyplot.axis('off')
    # pyplot.subplot(234), pyplot.imshow(iopenclosedialtedilate, 'gray'), pyplot.title('open-close-dilate-dilate'), pyplot.axis('off')
    # pyplot.subplot(235), pyplot.imshow(iopenclosedilatedilateopen, 'gray'), pyplot.title('open-close-dilate-dilate-open'), pyplot.axis('off')

    image, contours, hier = cv2.findContours(iopenclosedilatedilateopen, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    houses = [contour for contour in contours if (0.8 < numpy.sqrt(cv2.contourArea(contour))/(cv2.arcLength(contour, True)/4) < 1.2 and cv2.contourArea(contour) > (35*35))]

    destination_image = rgb_image.copy()
    cv2.drawContours(destination_image, houses, -1, (255, 0, 0), 5)

    if logger.VERBOSE_MODE:
        pyplot.figure()
        pyplot.imshow(destination_image), pyplot.title('There are ' + str(len(houses)) + ' houses'), pyplot.axis('off')
        pyplot.show()

    r, g, b = cv2.split(destination_image)
    destination_image = cv2.merge([b, g, r])
    cv2.imwrite(destination_image_path, destination_image)

if __name__ == '__main__':
    house_detection('./source/Test (7-1).jpg', './destination/Test (7-1).jpg')
    raw_input('Press Enter to Exit...')
