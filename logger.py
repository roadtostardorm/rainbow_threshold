import datetime

TIME_STAMP_FORMAT = '%m.%d %H:%M:%S:%f'

# Debug setting. Constant variable definition
VERBOSE_MODE = False


def debug(debug_object):
    """Print debug_object in debug mode"""

    if VERBOSE_MODE:
        dt = datetime.datetime.now()
        print '[DEBUG][' + dt.strftime(TIME_STAMP_FORMAT)[:-2] + ']' + str(debug_object)


def log(log_object):
    """Print log_object"""

    dt = datetime.datetime.now()
    print '[ LOG ][' + dt.strftime(TIME_STAMP_FORMAT)[:-2] + ']' + str(log_object)


def error(error_object):
    """Print log_object"""

    dt = datetime.datetime.now()
    print '[ERROR][' + dt.strftime(TIME_STAMP_FORMAT)[:-2] + ']' + str(error_object)
