from commandline_parser import *
import logger
import house_detection


def rainbow_threshold():
    args = commandline_parser()

    image_names = [f for f in os.listdir(args.source) if os.path.isfile(os.path.join(args.source, f))]
    logger.debug(image_names)

    logger.log('Begin Image Detection Processing')
    for image_name in image_names:
        source_path = os.path.join(args.source, image_name)
        destination_path = os.path.join(args.destination, image_name)
        logger.debug('Begin Processing ' + image_name + '...')

        house_detection.house_detection(source_path, destination_path)

        logger.debug('Finish Processing ' + image_name + '...')

    logger.log('Finish Image Detection Processing')

if __name__ == '__main__':
    rainbow_threshold()
