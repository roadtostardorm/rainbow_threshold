import os
import sys
import logger
import argparse


def __parse_input__():
    """ Create commandline parser and its required, optional parameters """
    parser = argparse.ArgumentParser(description='Process and Apply found housing in Picture')
    parser.add_argument('source', help='required source folder')
    parser.add_argument('destination', help='required destination folder')
    parser.add_argument('-v', '--verbose', action='store_true', help='more logging information')
    parser.add_argument('--version', action='version', version='%(prog)s 0.0.1')

    return parser


def commandline_parser():
    """ Takes care of input from commandline and take action accordingly """
    parser = __parse_input__()
    args = parser.parse_args()

    # enter verbose mode
    if args.verbose:
        logger.debug('Enter Verbose Mode')
    logger.VERBOSE_MODE = args.verbose

    # handles user input source folder
    args.source = os.path.abspath(args.source)
    if not os.path.exists(args.source):
        logger.error('Source Folder Does Not Exist')
        raise sys.exit('Source Folder Does Not Exist')

    # handles user input destination folder
    args.destination = os.path.abspath(args.destination)
    if not os.path.exists(args.destination):
        os.makedirs(args.destination)
        logger.log('Destination Folder Created')

    return args


if __name__ == '__main__':
    commandline_parser()
